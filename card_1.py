#카드 놀이에 사용될 카드 한벌을 나타내는 클래스
import collections

Card = collections.namedtuple('Card', ['rank', 'suit']) #메서드를 가지지 않는 일련의 속성으로 구성된 클래스
suit_values = dict(spades=3, hearts=2, diamonds=1, clubs=0) #순위를 정한다.

#카드 클래스
class FrenchDeck:
    ranks = [str(n) for n in range(2, 11)] + list('JQKA')
    suits = 'spades diamonds clubs hearts'.split()

    def __init__(self):
        self._cards = [Card(rank,suit) for suit in self.suits
                       for rank in self.ranks]

    def __len__(self): #len 기능 사용시 나타내는 메소드 수정
        return len(self._cards)

    def __getitem__(self, item): #이 클래스의 일부분을 가져올대 사용하기 위해서
        return self._cards[item]

def spades_high(card):
    rank_value = FrenchDeck.ranks.index(card.rank)
    return rank_value * len(suit_values) + suit_values[card.suit]

if __name__ == "__main__": #해당 모듈이 임포트 된 경우가 아니라 인터프리터에 실행된 경우만 돌리는 구문
    deck = FrenchDeck()
    for card in sorted(deck, key=spades_high):
        print(card)