symbols = "$¢£¥€¤"
t1 = tuple(ord(symbol) for symbol in symbols)
print(t1)
# 제너레이터 표현식이 함수에 보내는 단 하나의 인수라면 괄호 아넹 또 괄호를 넣을 필요는 없다.

import array
t2 = array.array("I",(ord(symbol) for symbol in symbols))
print(t2)
# 배열 생성자는 인수를 두 개 받으므로 제너레이터 표현식 앞 뒤에 반드시 괄호를 넣어야 한다.
# 배열 생성자의 첫번째 인수는 배열에 들어갈 숫자를 저장할 자료형으로 지정한다.