symbols = '$¢£¥€¤'
codes = [] #기본적인 방법
for symbol in symbols:
    codes.append(ord(symbol))

print(codes)

codes2 = [ord(symbol) for symbol in symbols] #지능형 리스트 방법
print(codes2)

x = 'ABC'
dummy = [ord(x) for x in x]
print(x)
print(dummy)