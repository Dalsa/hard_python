from math import hypot

class Vector:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __repr__(self): #객체를 문자열로 표현하게 위해 repr()에 호출됨..
        return "Vector(%r, %r)" % (self.x, self.y)

    def __abs__(self): #
        return hypot(self.x, self.y)

    def __bool__(self):
        return bool(abs(self))

    def __add__(self, other): # + 연산자의 역할을 불러온다.
        x = self.x + other.x
        y = self.y + other.y
        return Vector(x,y)

    def __mul__(self, scaler): # * 연산자의 역할을 불러온다.
        return Vector(self.x * scaler, self.y * scaler)


if __name__ = "__main__":
    print("잘생김")